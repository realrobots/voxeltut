using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeGenerator : MonoBehaviour
{

    private float tUnit = 0.25f;
    private Vector2 tDirt = new Vector2(0, 3);
    private Vector2 tGrass = new Vector2(1, 3);
    private Vector2 tGrassEdge = new Vector2(2, 3);


    // This first list contains every vertex of the mesh that we are going to render
    public List<Vector3> newVertices = new List<Vector3>();

    // The triangles tell Unity how to build each section of the mesh joining
    // the vertices
    public List<int> newTriangles = new List<int>();

    // The UV list is unimportant right now but it tells Unity how the texture is
    // aligned on each polygon
    public List<Vector2> newUV = new List<Vector2>();


    // A mesh is made up of the vertices, triangles and UVs we are going to define,
    // after we make them up we'll save them as this mesh
    private Mesh mesh;

    int chunkSize = 16;
    private int[,,] terrain;

    private int squareCount;

    FastNoise noise;

    // Start is called before the first frame update
    void Start()
    {

        noise = new FastNoise();
        noise.SetSeed(123);
        noise.SetFrequency(0.055f);
        noise.SetNoiseType(FastNoise.NoiseType.SimplexFractal); // Set the desired noise type

        mesh = GetComponent<MeshFilter>().mesh;

        GenerateTerrain();

        GenerateChunkMesh();

        UpdateMesh();
    }

    void GenerateTerrain()
    {


        terrain = new int[chunkSize, chunkSize, chunkSize];
        for (int x = 0; x < chunkSize; x++)
        {
            for (int z = 0; z < chunkSize; z++)
            {
                for (int y = 0; y < chunkSize; y++)
                {
                    float val = noise.GetNoise(x, y, z);
                    if (val < 0) val = 0;
                    else val = 1;
                    Debug.Log(val);
                    terrain[x, y, z] = Mathf.RoundToInt(val);


                }
            }
        }
    }

    void GenerateChunkMesh()
    {
        for (int x = 0; x < chunkSize; x++)
        {
            for (int z = 0; z < chunkSize; z++)
            {
                for (int y = 0; y < chunkSize; y++)
                {
                    Debug.Log(x + ", " + y + ", " + z);
                    try{
                        GenerateCube(x, y, z, tDirt);
                    } finally{

                    }
                }
            }
        }

    }

    void GenerateCube(int x, int y, int z, Vector2 texture)
    {

        if (terrain[x, y, z] == 0)
        {
            return;
        }

        // FRONT
        if (z > 0)
        {
            if (terrain[x, y, z - 1] == 0)
            {
                newVertices.Add(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));

                AddTriangles();
                
                    AddUVs(texture);
                
                squareCount++;
            }
        }

        // TOP
        if (y < chunkSize - 1)
        {
            if (terrain[x, y + 1, z] == 0)
            {
                newVertices.Add(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));

                AddTriangles();
                AddUVs(tGrass);
                squareCount++;
            }
        }

        // BACK
        if (z < chunkSize - 1)
        {
            if (terrain[x, y, z + 1] == 0)
            {
                newVertices.Add(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));

                AddTriangles();
               
                    AddUVs(texture);
                
                squareCount++;
            }
        }

        // BOTTOM
        if (y > 0)
        {
            if (terrain[x, y - 1, z] == 0)
            {
                newVertices.Add(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));

                AddTriangles();
                AddUVs(texture);
                squareCount++;
            }
        }

        // LEFT
        if (x > 0)
        {
            if (terrain[x - 1, y, z] == 0)
            {
                newVertices.Add(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
                newVertices.Add(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));

                AddTriangles();
                
                    AddUVs(texture);
                
                squareCount++;
            }
        }

        // RIGHT
        if (x < chunkSize)
        {
            if (terrain[x + 1, y, z] == 0)
            {
                newVertices.Add(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
                newVertices.Add(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));

                AddTriangles();
                
                    AddUVs(texture);
                
                squareCount++;
            }
        }
    }

    void AddUVs(Vector2 texture)
    {
        newUV.Add(new Vector2(tUnit * texture.x, tUnit * texture.y + tUnit));
        newUV.Add(new Vector2(tUnit * texture.x + tUnit, tUnit * texture.y + tUnit));
        newUV.Add(new Vector2(tUnit * texture.x + tUnit, tUnit * texture.y));
        newUV.Add(new Vector2(tUnit * texture.x, tUnit * texture.y));
    }

    void AddTriangles()
    {
        newTriangles.Add((squareCount * 4) + 0);
        newTriangles.Add((squareCount * 4) + 1);
        newTriangles.Add((squareCount * 4) + 3);
        newTriangles.Add((squareCount * 4) + 1);
        newTriangles.Add((squareCount * 4) + 2);
        newTriangles.Add((squareCount * 4) + 3);
    }

    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = newVertices.ToArray();
        mesh.triangles = newTriangles.ToArray();
        mesh.uv = newUV.ToArray(); // add this line to the code here
        mesh.Optimize();
        mesh.RecalculateNormals();

        gameObject.GetComponent<MeshCollider>().sharedMesh = mesh;

        squareCount = 0;
        newVertices.Clear();
        newTriangles.Clear();
        newUV.Clear();
    }

    public void DeleteBlock(Vector3 pos)
    {
        Debug.Log("Deleting: " + pos);
        terrain[Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y), Mathf.RoundToInt(pos.z)] = 0;
        GenerateChunkMesh();

        UpdateMesh();
    }

    // Update is called once per frame
    void Update()
    {


    }
}